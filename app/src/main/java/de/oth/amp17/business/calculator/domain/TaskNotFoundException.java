package de.oth.amp17.business.calculator.domain;

import javax.ejb.ApplicationException;

@ApplicationException
public class TaskNotFoundException extends RuntimeException {

    public TaskNotFoundException(String correlationIdentifier, Throwable cause) {
        super("Task not found, correlationIdentifier: " + correlationIdentifier, cause);
    }
}
