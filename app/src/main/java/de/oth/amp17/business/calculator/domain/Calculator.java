package de.oth.amp17.business.calculator.domain;

import de.oth.amp17.business.calculator.domain.function.CalculatorFunction;
import de.oth.amp17.business.calculator.domain.function.Operation;

import javax.enterprise.inject.Any;
import javax.enterprise.inject.Instance;
import javax.enterprise.util.AnnotationLiteral;
import javax.inject.Inject;

public class Calculator {

    private Instance<CalculatorFunction> functions;

    @Inject
    public Calculator(
            @Any Instance<CalculatorFunction> functions
    ) {
        this.functions = functions;
    }

    public void handle(SubmittedTask submittedTask) {
        try {
            double result = doHandle(submittedTask.asTask());
            submittedTask.completed(result);
        } catch (Exception ex) {
            submittedTask.failed(ex.getMessage());
        }
    }

    public Result handle(Task task) {
        double result = doHandle(task);
        return Result.completed(task, result);
    }

    private double doHandle(Task task) {
        CalculatorFunction function = selectFunction(task);
        return function.apply(task.x(), task.y());
    }

    private CalculatorFunction selectFunction(Task task) {
        return functions.select(new OperationQualifier(task.operation())).get();
    }

    private static class OperationQualifier extends AnnotationLiteral<Operation> implements Operation {

        private String name;

        private OperationQualifier(String name) {
            this.name = name;
        }

        @Override
        public String name() {
            return name;
        }
    }

}
