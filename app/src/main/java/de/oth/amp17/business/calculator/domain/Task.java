package de.oth.amp17.business.calculator.domain;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Entity;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@Embeddable
public class Task {

    @XmlAttribute
    @Column(name = "OPERATION")
    private String operation;

    @XmlElement
    @Column(name = "OP_X")
    private double x;

    @XmlElement
    @Column(name = "OP_Y")
    private double y;

    /**
     * Mapper only.
     */
    protected Task() {
    }

    public Task(String operation, double x, double y) {
        this.operation = operation;
        this.x = x;
        this.y = y;
    }

    public String operation() {
        return operation;
    }

    public double x() {
        return x;
    }

    public double y() {
        return y;
    }
}
