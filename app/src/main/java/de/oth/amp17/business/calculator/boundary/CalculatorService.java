package de.oth.amp17.business.calculator.boundary;

import de.oth.amp17.business.calculator.domain.Calculator;
import de.oth.amp17.business.calculator.domain.Result;
import de.oth.amp17.business.calculator.domain.Task;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

import static javax.jws.soap.SOAPBinding.ParameterStyle.BARE;

@Stateless
@WebService(
        name = "calculator",
        serviceName = "calculator",
        targetNamespace = "http://www.oth.de/amp17/calculator/message"
)
@SOAPBinding(parameterStyle = BARE)
public class CalculatorService {

    @Inject
    private Calculator calculator;

    @WebMethod
    public Result handle(Task task) {
        return calculator.handle(task);
    }

}
