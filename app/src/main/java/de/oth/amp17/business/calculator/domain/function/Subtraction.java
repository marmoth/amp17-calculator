package de.oth.amp17.business.calculator.domain.function;

@Operation(name = "sub")
public class Subtraction implements CalculatorFunction {

    @Override
    public double apply(double x, double y) {
        return x - y;
    }
}
