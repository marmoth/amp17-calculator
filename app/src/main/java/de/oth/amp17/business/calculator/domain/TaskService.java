package de.oth.amp17.business.calculator.domain;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.UUID;

@ApplicationScoped
public class TaskService {

    @Inject
    private SubmittedTaskRepository repository;

    @Inject
    private Calculator calculator;

    public String submit(Task task) {
        String correlationIdentifier = UUID.randomUUID().toString();

        SubmittedTask submittedTask = new SubmittedTask(correlationIdentifier, task);
        repository.add(submittedTask);

        return correlationIdentifier;
    }

    public void completeTasks() {
        repository.pending().forEach(calculator::handle);
    }

}
