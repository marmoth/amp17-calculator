package de.oth.amp17.business.calculator.domain;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

import static javax.ejb.TransactionAttributeType.REQUIRED;

@Stateless
@TransactionAttribute(REQUIRED)
public class SubmittedTaskRepository {

    @PersistenceContext
    private EntityManager em;

    public void add(SubmittedTask task) {
        em.persist(task);
    }

    public SubmittedTask findByCorrelationIdentifier(String cid) throws TaskNotFoundException {
        TypedQuery<SubmittedTask> query = em.createNamedQuery(
                SubmittedTask.QUERY_findByCorrelationIdentifier, SubmittedTask.class);
        query.setParameter(SubmittedTask.PARAM_correlationIdentifier, cid);
        try {
            return query.getSingleResult();
        } catch (NoResultException ex) {
            throw new TaskNotFoundException(cid, ex);
        }
    }

    public List<SubmittedTask> pending() {
        TypedQuery<SubmittedTask> query = em.createNamedQuery(
                SubmittedTask.QUERY_byState, SubmittedTask.class);
        query.setParameter(SubmittedTask.PARAM_state, SubmittedTask.State.PENDING);
        return query.getResultList();
    }

}
