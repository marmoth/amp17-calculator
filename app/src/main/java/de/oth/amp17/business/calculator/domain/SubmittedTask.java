package de.oth.amp17.business.calculator.domain;

import javax.persistence.*;

@Entity

@NamedQueries({
    @NamedQuery(name = SubmittedTask.QUERY_findByCorrelationIdentifier,
            query = "select t from SubmittedTask t where t.correlationIdentifier = :cid"),
    @NamedQuery(name = SubmittedTask.QUERY_byState,
            query = "select t from SubmittedTask t where t.state = :state")
})
public class SubmittedTask {

    public static final String QUERY_findByCorrelationIdentifier = "SubmittedTask.byCorrelationIdentifier";

    public static final String QUERY_byState = "SubmittedTask.pending";

    public static final String PARAM_correlationIdentifier = "cid";

    public static final String PARAM_state = "state";

    enum State {
        PENDING, COMPLETED, FAILED;
    }

    @Id
    @GeneratedValue
    @Column(name = "ID")
    private Long surrogateKey;

    @Column(name="C_ID")
    private String correlationIdentifier;

    @Embedded
    private Task task;

    @Column(name = "STATE")
    @Enumerated
    private State state = State.PENDING;

    @Column(name = "RESULT_VALUE")
    private Double result;

    @Column(name = "ERROR_MESSAGE")
    private String errorMessage;

    /**
     * Mapper only.
     */
    protected SubmittedTask() {
    }

    public SubmittedTask(String correlationIdentifier, Task task) {
        this.correlationIdentifier = correlationIdentifier;
        this.task = task;
    }

    public String operation() {
        return task.operation();
    }

    public double x() {
        return task.x();
    }

    public double y() {
        return task.y();
    }

    Task asTask() {
        return task;
    }

    public boolean isPending() {
        return State.PENDING == state;
    }

    public void completed(double result) {
        this.state = State.COMPLETED;
        this.result = result;
    }

    public void failed(String message) {
        this.state = State.FAILED;
        this.errorMessage = message;
    }

    public Result result() {
        switch (state) {
            case COMPLETED:
                return Result.completed(task, result);
            case FAILED:
                return Result.failed(task, errorMessage);
            default:
                throw new IllegalStateException();
        }
    }
}
