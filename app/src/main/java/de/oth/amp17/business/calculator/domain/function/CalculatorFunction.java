package de.oth.amp17.business.calculator.domain.function;

public interface CalculatorFunction {

    double apply(double x, double y);

}
