package de.oth.amp17.business.calculator.boundary;

import de.oth.amp17.business.calculator.domain.*;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.net.URI;
import java.util.logging.Level;
import java.util.logging.Logger;

import static javax.ejb.TransactionAttributeType.REQUIRED;
import static javax.ws.rs.core.Response.Status.NOT_FOUND;
import static javax.ws.rs.core.Response.created;
import static javax.ws.rs.core.Response.noContent;
import static javax.ws.rs.core.Response.ok;

@Stateless
@Path("rs/calculator")
@Consumes(MediaType.APPLICATION_XML)
@Produces(MediaType.APPLICATION_XML)
public class CalculatorResource {

    @Inject
    private TaskService taskService;

    @Inject
    private SubmittedTaskRepository repository;

    @Context
    private UriInfo uriInfo;

    @POST
    public Response submitTask(Task task) {
        String cid = taskService.submit(task);

        URI taskUri = uriInfo.getRequestUriBuilder().segment(cid).build();

        return created(taskUri).build();
    }

    @Path("{cid}")
    @GET
    public Response getResult(@PathParam("cid") String cid) {
        try {
            SubmittedTask task = repository.findByCorrelationIdentifier(cid);

            if (task.isPending()) {
                return noContent().build();
            }

            return ok(task.result()).build();
        } catch (TaskNotFoundException ex) {
            Logger.getLogger(getClass().getName()).log(Level.INFO, ex.getMessage());
            return Response.status(NOT_FOUND).build();
        }

    }

    @GET
    @Path("triggerTaskCompletion")
    @TransactionAttribute(REQUIRED)
    public Response triggerTaskCompletion() {
        taskService.completeTasks();
        return ok().build();
    }

}
