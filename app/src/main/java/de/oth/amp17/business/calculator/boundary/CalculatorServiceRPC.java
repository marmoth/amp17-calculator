package de.oth.amp17.business.calculator.boundary;

import de.oth.amp17.business.calculator.domain.Calculator;
import de.oth.amp17.business.calculator.domain.Result;
import de.oth.amp17.business.calculator.domain.Task;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

import static javax.jws.soap.SOAPBinding.Style.RPC;

@Stateless
@WebService(
        name = "rpcCalculator",
        serviceName = "calculator",
        targetNamespace = "http://www.oth.de/amp17/calculator/rpc"
)
@SOAPBinding(style = RPC)
public class CalculatorServiceRPC {

    @Inject
    private Calculator calculator;

    @WebMethod
    public double add(double x, double y) {
        return handleTask("add", x, y);
    }

    @WebMethod
    public double sub(double x, double y) {
        return handleTask("add", x, y);
    }

    private double handleTask(String operation, double x, double y) {
        Task task = new Task(operation, x, y);
        Result result = calculator.handle(task);
        return result.value();
    }

}
