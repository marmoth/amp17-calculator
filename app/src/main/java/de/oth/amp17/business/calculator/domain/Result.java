package de.oth.amp17.business.calculator.domain;

import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;

import static javax.xml.bind.annotation.XmlAccessType.FIELD;

@XmlRootElement
@XmlAccessorType(value = FIELD)
public class Result {

    @XmlElement
    private Double value;

    @XmlElement
    private String message;

    @XmlElementRef
    private Task task;

    /**
     * Mapper only.
     */
    protected Result() {
    }

    private Result(Task task, double value) {
        this.task = task;
        this.value = value;
    }

    public boolean isTaskCompleted() {
        return value != null;
    }

    public Task task() {
        return task;
    }

    public double value() {
        return value;
    }

    public static Result completed(Task task, double value) {
        return new Result(task, value);
    }

    public static Result failed(Task task, String message) {
        Result result = new Result();
        result.task = task;
        result.message = message;
        return result;
    }
}
