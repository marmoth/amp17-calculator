package de.oth.amp17.business.calculator.domain.function;

@Operation(name = "add")
public class Addition implements CalculatorFunction {

    @Override
    public double apply(double x, double y) {
        return x + y;
    }
}
