package de.oth.amp17.scratch;

import javax.enterprise.context.spi.CreationalContext;
import javax.enterprise.inject.spi.Bean;
import javax.enterprise.inject.spi.BeanManager;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.lang.reflect.Method;
import java.util.Set;

@Path("testcdi")
public class TestCdiRS {

    @Inject
    private BeanManager beanManager;

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String testNamed() throws Exception {

        Set<Bean<?>> beans = beanManager.getBeans("simpleCalc");

        Bean<?> bean = beans.stream().findFirst().get();

        CreationalContext<?> ctx = beanManager.createCreationalContext(bean);

        Object resolved = beanManager.getReference(bean, Object.class, ctx);

        Method apply = resolved.getClass().getMethod("add", double.class, double.class);


//        return cf.getClass().getName();

        return apply.invoke(resolved, 4, 2).toString();

    }

}
