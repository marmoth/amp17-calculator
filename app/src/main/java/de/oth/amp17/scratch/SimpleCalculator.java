package de.oth.amp17.scratch;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;

@Named("simpleCalc")
@ApplicationScoped
public class SimpleCalculator {

    public double add(double x, double y) {
        return x + y;
    }

}
