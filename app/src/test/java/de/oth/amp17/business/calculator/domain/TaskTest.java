package de.oth.amp17.business.calculator.domain;

import org.junit.Test;

import javax.xml.bind.JAXB;

import java.io.StringWriter;

import static org.junit.Assert.*;

public class TaskTest {

    @Test
    public void marshall() {

        StringWriter writer = new StringWriter();
        JAXB.marshal(new Task("add", 3.2, 2.3), writer);
        System.out.println(writer);

    }

}