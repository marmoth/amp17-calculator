
package de.oth.amp17.calculator.message;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the de.oth.amp17.calculator.message package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Task_QNAME = new QName("http://www.oth.de/amp17/calculator/message", "task");
    private final static QName _Result_QNAME = new QName("http://www.oth.de/amp17/calculator/message", "result");
    private final static QName _HandleResponse_QNAME = new QName("http://www.oth.de/amp17/calculator/message", "handleResponse");
    private final static QName _Handle_QNAME = new QName("http://www.oth.de/amp17/calculator/message", "handle");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: de.oth.amp17.calculator.message
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Result }
     * 
     */
    public Result createResult() {
        return new Result();
    }

    /**
     * Create an instance of {@link Task }
     * 
     */
    public Task createTask() {
        return new Task();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Task }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.oth.de/amp17/calculator/message", name = "task")
    public JAXBElement<Task> createTask(Task value) {
        return new JAXBElement<Task>(_Task_QNAME, Task.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Result }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.oth.de/amp17/calculator/message", name = "result")
    public JAXBElement<Result> createResult(Result value) {
        return new JAXBElement<Result>(_Result_QNAME, Result.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Result }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.oth.de/amp17/calculator/message", name = "handleResponse")
    public JAXBElement<Result> createHandleResponse(Result value) {
        return new JAXBElement<Result>(_HandleResponse_QNAME, Result.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Task }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.oth.de/amp17/calculator/message", name = "handle")
    public JAXBElement<Task> createHandle(Task value) {
        return new JAXBElement<Task>(_Handle_QNAME, Task.class, null, value);
    }

}
