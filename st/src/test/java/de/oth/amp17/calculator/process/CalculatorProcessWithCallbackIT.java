package de.oth.amp17.calculator.process;

import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonStructure;
import javax.json.JsonWriter;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import java.io.StringWriter;
import java.net.URI;
import java.util.concurrent.TimeUnit;

import static de.oth.amp17.calculator.process.CalculatorProcessIT.URI_SUBMIT;
import static org.junit.Assert.assertEquals;

public class CalculatorProcessWithCallbackIT {

//    private static final String URI_CONTEXT = "/async-op-0.1-SNAPSHOT";
//
//    private static final String URI_SERVER = "http://localhost:8080";
//
//    private static final URI URI_BASE = URI.create(URI_SERVER + URI_CONTEXT + "/api/");
//
//    private static final URI URI_SUBMIT = URI_BASE.resolve("submit");

    private URI baseUri;
    private HttpServer server;

    @Test
    public void executeProcessWithCallbackURI() throws Exception {

        Client client = ClientBuilder.newClient();

        Response response = client.target(URI_SUBMIT).request().
                post(taskEntity("add", 3, 5));
        assertEquals(Response.Status.NO_CONTENT.getStatusCode(), response.getStatus());

        JsonObject result = ResultsCollector.RESULTS.poll(5, TimeUnit.SECONDS);

        assertEquals(
                8,
                result.getInt("result"));

    }

    private Entity taskEntity(String op, int x, int y) {
        JsonObject result = Json.createObjectBuilder().
                add("op", op).
                add("x", x).
                add("y", y).
                add("callbackURI", UriBuilder.fromUri(baseUri).segment("callback").build().toString()).
                build();
        return Entity.entity(result, MediaType.APPLICATION_JSON_TYPE);
    }


    private void dump(JsonStructure structure) {
        StringWriter out = new StringWriter();
        try (JsonWriter jsonWriter = Json.createWriter(out)) {
            jsonWriter.write(structure);
        }
        System.out.println(out.toString());
    }


    @Before
    public void startEndpoint() throws Exception {
        baseUri = UriBuilder.fromUri("http://localhost/").port(9978).build();
        ResourceConfig config = new ResourceConfig(ResultsCollector.class);
        server = GrizzlyHttpServerFactory.createHttpServer(baseUri, config);
        server.start();
    }

    @After
    public void stopEndpoint() throws Exception {
        server.shutdown();
    }

}
