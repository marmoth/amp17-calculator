package de.oth.amp17.calculator.resource;

import org.hamcrest.core.Is;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import java.net.URI;

import static javax.ws.rs.core.Response.Status.CREATED;
import static javax.ws.rs.core.Response.Status.NO_CONTENT;
import static javax.ws.rs.core.Response.Status.OK;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

public class CalculatorResourceIT {

    static final String URI_CONTEXT = "/calculator";

    static final String URI_SERVER = "http://localhost:8080";

    static final URI URI_BASE = URI.create(URI_SERVER + URI_CONTEXT + "/api/rs/calculator");

    private Client client;

    @Before
    public void setup() {
        client = ClientBuilder.newClient();
    }

    @After
    public void tearDown() {
        client.close();
    }

    @Test
    public void handleTask() {

        Task task = new Task("add", 3, 5);

        Response response = client.target(URI_BASE).request()
                .post(Entity.entity(task, MediaType.APPLICATION_XML));
        assertThat(response.getStatus(), is(CREATED.getStatusCode()));

        String location = response.getHeaderString("Location");
        assertNotNull("location header required", location);

        response = client.target(location).request().get();
        assertThat(response.getStatus(), is(NO_CONTENT.getStatusCode()));

        response = client.
                target(UriBuilder.fromUri(URI_BASE).segment("triggerTaskCompletion").build()).
                request().get();

        assertThat(response.getStatus(), is(OK.getStatusCode()));

        Result result = client.target(location).request().get(Result.class);
        assertThat(result.value(), is(8.0));
    }

}
