package de.oth.amp17.calculator.process;

import javax.json.JsonObject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

@Path("/callback")
public class ResultsCollector {

    public static final BlockingQueue<JsonObject> RESULTS = new ArrayBlockingQueue<>(100);

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public void collect(JsonObject result) {
        System.out.println("received result = " + result);
        RESULTS.offer(result);
    }

}
