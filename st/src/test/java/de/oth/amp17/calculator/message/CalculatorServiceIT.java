package de.oth.amp17.calculator.message;

import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class CalculatorServiceIT {

    @Test
    public void add() {

        Calculator_Service service = new Calculator_Service();
        Calculator calculator = service.getCalculatorPort();

        Task task = new Task();
        task.setOperation("add");
        task.setX(3);
        task.setY(5);

        Result result = calculator.handle(task);

        assertThat(result.getValue(), is(8.0));

    }

}
