package de.oth.amp17.calculator.resource;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Task {

    @XmlAttribute
    private String operation;

    @XmlElement
    private double x;

    @XmlElement
    private double y;

    /**
     * Mapper only.
     */
    protected Task() {
    }

    public Task(String operation, double x, double y) {
        this.operation = operation;
        this.x = x;
        this.y = y;
    }

    public String operation() {
        return operation;
    }

    public double x() {
        return x;
    }

    public double y() {
        return y;
    }
}
