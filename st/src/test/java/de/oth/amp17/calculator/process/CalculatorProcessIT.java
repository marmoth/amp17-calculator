package de.oth.amp17.calculator.process;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.json.*;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.StringWriter;
import java.net.URI;

import static javax.ws.rs.core.Response.Status.NO_CONTENT;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;

/**
 *
 * @author mam02072
 */
public class CalculatorProcessIT {

    static final String MSG_DIV_BY_ZERO = "Fehler: Division durch null";

    static final String URI_CONTEXT = "/calculator";
    
    static final String URI_SERVER = "http://localhost:8080";
    
    static final URI URI_BASE = URI.create(URI_SERVER + URI_CONTEXT + "/api/");
    
    static final URI URI_SUBMIT = URI_BASE.resolve("submit");

    static final URI URI_RESULTS = URI_BASE.resolve("results");

    private Client client;

    @Before
    public void setup() {
        client = ClientBuilder.newClient();
        
        //clear result buffer
        doFetchResults();
    }

    @After
    public void tearDown() {
        client.close();
    }
    
    @Test
    public void processHandlesAddition() {
        assertProcessHandlesTask("add", 2, 3, 5);
    }
    
    @Test
    public void processHandlesDivision_Div1() {
        assertProcessHandlesTask("div1", 4, 2, 2);
    }
    
    @Test
    public void processHandlesDivision_Div2() {
        assertProcessHandlesTask("div2", 4, 2, 2);
    }
    
    @Test
    public void processReportsDivisionByZero_Div1() {
        assertProcessReportsError("div1", 2, 0, MSG_DIV_BY_ZERO);
    }
    
    @Test
    public void processReportsDivisionByZero_Div2() {
        assertProcessReportsError("div2", 2, 0, MSG_DIV_BY_ZERO);
    }
    
    @Test
    public void processHandlesMultiplication() {
        assertProcessHandlesTask("mul", 2, 3, 6);
    }

    private void assertProcessHandlesTask(String op, int x, int y, double expectedResult) {
        JsonArray results = submitTask(op, x, y);

        JsonNumber result = results.
                getJsonObject(0).
                getJsonNumber("result");
        assertEquals("task result", expectedResult, result.doubleValue(), 0);
    }
    
    private void assertProcessReportsError(String op, int x, int y, String expectedErrorMessage) {
        JsonArray results = submitTask(op, x, y);

        JsonString result = results.
                getJsonObject(0).
                getJsonString("error");
        assertEquals("error message", expectedErrorMessage, result.getString());
    }

    private JsonArray submitTask(String op, int x, int y) {
        postAndAssertResponseStatus(
                URI_SUBMIT,
                taskEntity(op, x, y),
                NO_CONTENT);
        JsonArray results = fetchResults();
        dump(results);
        return results;
    }

    private void postAndAssertResponseStatus(URI uri, Entity entity, Response.Status status) {
        Response response = client.target(uri).request().
                post(entity);
        assertStatus(status, response);
    }

    private JsonArray fetchResults() {
        JsonArray results = doFetchResults();
        if (results.isEmpty()) {
            try {
                Thread.sleep(500);
            } catch (InterruptedException ex) {
                throw new RuntimeException(ex);
            }
            results = doFetchResults();
            assertFalse("non empty result expected", results.isEmpty());
        }
        return results;
    }

    private JsonArray doFetchResults() {
        JsonArray results = client.target(URI_RESULTS).
                request(MediaType.APPLICATION_JSON).
                get(JsonArray.class);
        return results;
    }

    private Entity taskEntity(String op, int x, int y) {
        JsonObject result = Json.createObjectBuilder().
                add("op", op).
                add("x", x).
                add("y", y).
                build();
        return Entity.entity(result, MediaType.APPLICATION_JSON_TYPE);
    }

    public void assertStatus(Response.Status status, Response response) {
        assertThat(response.getStatus(), is(status.getStatusCode()));
    }

    private void dump(JsonStructure structure) {
        StringWriter out = new StringWriter();
        try (JsonWriter jsonWriter = Json.createWriter(out)) {
            jsonWriter.write(structure);
        }
        System.out.println(out.toString());
    }
}
