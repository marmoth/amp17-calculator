package de.oth.amp17.calculator.rpc;

import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class RpcCalculatorIT {

    @Test
    public void add() {

        Calculator calculator = new Calculator();
        RpcCalculator rpcCalculatorPort = calculator.getRpcCalculatorPort();

        double result = rpcCalculatorPort.add(3, 5);

        assertThat(result, is(8.0));

    }

}
