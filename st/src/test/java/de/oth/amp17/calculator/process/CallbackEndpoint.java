package de.oth.amp17.calculator.process;

import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;

import javax.json.JsonObject;
import javax.ws.rs.core.UriBuilder;
import java.net.URI;
import java.util.concurrent.TimeUnit;

public class CallbackEndpoint {

    public static void main(String[] argv) throws Exception {


        URI baseUri = UriBuilder.fromUri("http://localhost/").port(9978).build();
        ResourceConfig config = new ResourceConfig(ResultsCollector.class);
        HttpServer server = GrizzlyHttpServerFactory.createHttpServer(baseUri, config);

        server.start();

        try {

            JsonObject result = ResultsCollector.RESULTS.poll(5, TimeUnit.MINUTES);
            System.out.printf("Result received: %s.%nShutting down endpoint...%n", result);

        } finally {
            server.shutdown(1, TimeUnit.SECONDS);
        }
    }

}
